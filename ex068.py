from random import randint
cont = 0
print('-=' * 10)
print('Jogo da adivinhação')
print('-=' * 10)
while True:
    computador = randint(1, 10)
    player = int(input('Digite um número: '))
    impar_par = str(input('Impar ou par [I/P]? ')).upper()[0]
    if ((player + computador) % 2) == 0:
        print('-=' * 30)
        print(f'Você digitou {player} e o computador {computador} a soma é {player + computador} e é PAR')
        if impar_par == 'P':
            print('-=' * 30)
            print('''Você ganhou! \nVamos jogar de novo...''')
            print('-=' * 30)
            cont += 1
        else:
            print('Perdeu!')
            break
    else:
        print(f'Você digitou {player} e o computador {computador} a soma é {player + computador} e é IMPAR')
        if impar_par == 'I':
            print('-=' * 30)
            print('''Você ganhou! \nVamos jogar de novo...''')
            print('-=' * 30)
            cont += 1
        else:
            print('Perdeu!')
            break
print('-' * 33)
print(f'Você ganhou {cont} vezes consecutiva(s)')
print('-' * 33)
